//#pragma once
#ifndef DVD_H
#define DVD_H
#include "Product.h"
#include <fstream>

class DVD : public Product
{
private:
    std::string director;
    double play_time;
    std::string genre;
public:
    DVD();
    //~DVD();
    DVD(std::string type, int id, std::string title, double cost, int qty,
        int qtySold, std::string director, double play_time, std::string genre);
    void writeNewProduct();
    void updateStockItem(int id, int n, std::string qty_arg);

    // Getters
    std::string get_director();
    double get_play_time();
    std::string get_genre();
  
    // Setters
    void set_director(std::string director);
    void set_play_time(double play_time);
    void set_genre(std::string genre);
};

#endif // DVD_H
