#include "Book.h"
#include <sstream>

Book::Book():Product()
{
    ISDN = "";
}
Book::Book(std::string type, int id, std::string title, double cost, int qty, int qtySold,
    std::string ISDN) : Product("Book", id, title, cost, qty, qtySold)
{
    this->ISDN = ISDN;
}

// Getter
std::string Book::get_ISDN() { return this->ISDN; }

// Setter
void Book::set_ISDN(std::string ISDN) { this->ISDN = ISDN; }

void Book::writeNewProduct()
{
    int id;
    std::cout << "Enter item id : ";
    std::cin >> id;

    bool found = false;
    std::ifstream inFile("stock.txt");
    std::string arr[6];
    std::string line;
    while (getline(inFile, line))
    {
        if (line.length() < 1)
        {
            break;
        }

        std::istringstream iss(line);
        for (int i = 0; i < (sizeof(arr) / sizeof(arr[0])); i++)
        {
            std::string subs;
            iss >> subs;
            arr[i] = subs;
        }

        if (std::to_string(id) == arr[1])
        {
            std::cout << "\n\tThe Product with id '" << id << "' is found already!\n";
            found = true;
            break;
        }
    }
    inFile.close();

    if (found == false)
    {
        std::string type = "Book";
        std::string title, ISDN;
        int qty, qtySold;
        double cost;

        std::cout << "Enter item title : ";
        std::cin >> title;
        std::cout << "Enter item cost : ";
        std::cin >> cost;
        std::cout << "Enter item Qty : ";
        std::cin >> qty;
        std::cout << "Enter item Qty Sold : ";
        std::cin >> qtySold;
        //=======================
        std::cout << "Enter ISDN : ";
        std::cin >> ISDN;
        

        std::ofstream outFile;
        outFile.open("stock.txt", std::ios::out | std::ios::app);

        outFile << type << "\t" << id << "\t" << title << "\t" << cost << "\t" << qty << "\t" << qtySold
            << "\t" << ISDN << "\n";

        outFile.close();
        std::cout << "\nProduct '" << title << "' written successfully!\n\n";
    }
}

void Book::updateStockItem(int id, int n, std::string qty_arg)
{
    bool found = false;
    std::ifstream inFile("stock.txt");
    std::ofstream tempFile("temp.txt");
    std::string arr[10];
    std::string line;
    while (getline(inFile, line))
    {
        if (line.length() < 1)
        {
            break;
        }

        std::istringstream iss(line);
        for (int i = 0; i < (sizeof(arr) / sizeof(arr[0])); i++)
        {
            std::string subs;
            iss >> subs;
            arr[i] = subs;
        }
        if (std::to_string(id) != arr[1] && line.length() > 1)
            tempFile << line << "\n";
        else if (std::to_string(id) == arr[1] && line.length() > 1)
        {
            int x = stoi(arr[4]);
            int y = stoi(arr[5]);
            found = true;
            if (qty_arg == "qty")
            {
                x += n;
                arr[4] = std::to_string(x);
            }
            else if (qty_arg == "qtySold")
            {
                x -= n;
                y += n;
                arr[4] = std::to_string(x);
                arr[5] = std::to_string(y);
            }
            tempFile << arr[0] << "\t" << arr[1] << "\t" << arr[2] << "\t" << arr[3] << "\t" << arr[4] << "\t" << arr[5]
                << "\t" << arr[6] << "\n";

        }
    }
    inFile.close();
    tempFile.close();

    //======================================

    if (found == true)
    {
        remove("stock.txt");
        int c = rename("temp.txt", "stock.txt");
    }
    else
    {
        std::cout << "\nThe item which has id '" << id << "' not found\n";
        remove("temp.txt");
    }
}

