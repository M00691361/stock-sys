#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "CD.h"
#include "DVD.h"
#include "Magazine.h"
#include "Book.h"
#include "Product.h"

TEST_CASE("test CD Constructor", "[CD()]")
{
	CD cd("CD", 5, "bestSongs", 12.0, 75, 0, "Jack", 6, 58.0, "Jaz");

	REQUIRE(cd.get_id() == 5);					// - 1
	REQUIRE(cd.get_title() == "bestSongs");		// - 2
	REQUIRE(cd.get_cost() == 12);				// - 3
	REQUIRE(cd.get_qty() == 75);				// - 4
	REQUIRE(cd.get_truck_count() == 6);			// - 5
	REQUIRE(cd.get_play_time() == 58);			// - 6
	REQUIRE(cd.get_artist() == "Jack");			// - 7
	REQUIRE(cd.get_genre() == "Jaz");			// - 8

	cd.set_cost(16);
	cd.set_play_time(59);
	cd.set_truck_count(8);

	REQUIRE(cd.get_truck_count() == 8);			// - 9
	REQUIRE(cd.get_cost() == 16);				// - 10
	REQUIRE(cd.get_play_time() == 59);			// - 11
}

TEST_CASE("test DVD Constructor", "[DVD()]")
{
	DVD dvd;
	dvd.set_type("DVD");
	dvd.set_director("Wiliam");
	dvd.set_play_time(92);
	dvd.set_genre("Comedy");

	REQUIRE(dvd.get_id() == 0);						// - 12
	REQUIRE(dvd.get_title() == "");					// - 13
	REQUIRE(dvd.get_type() == "DVD");				// - 14
	REQUIRE(dvd.get_director() == "Wiliam");		// - 15
	REQUIRE(dvd.get_play_time() == 92);				// - 16
	REQUIRE(dvd.get_genre() == "Comedy");			// - 17
}

TEST_CASE("test Magazine Constructor", "[Magazine()]")
{
	Magazine item("xxx", 8, "COVID", 14, 74, 1, 60012);
	//Magazine item;
	Product* prod = &item;

	prod->set_type("Magazine");
	prod->set_id(1001);
	prod->set_title("COVID19");
	prod->set_cost(12);
	prod->set_qtySold(2);

	REQUIRE(item.get_issue_number() == 60012);		// - 18
	REQUIRE(item.get_issue_number() == 60012);		// - 19
	item.set_issue_number(40122);
	REQUIRE(item.get_issue_number() == 40122);		// - 20
	REQUIRE(item.get_title() == "COVID19");			// - 21
	REQUIRE(prod->get_title() == "COVID19");		// - 22

	REQUIRE(prod->get_type() == "Magazine");		// - 23
	REQUIRE(item.get_cost() == 12);					// - 24
	REQUIRE(prod->get_qty() == 74);					// - 25
	REQUIRE(prod->get_qtySold() == 2);				// - 26
}

TEST_CASE("test Book Constructor", "[Book()]")
{
	Book book;
	book.set_type("Book");
	book.set_ISDN("AD009");
	book.set_qty(150);

	REQUIRE(book.get_id() == 0);				// - 27
	REQUIRE(book.get_title() == "");			// - 28
	REQUIRE(book.get_type() == "Book");			// - 29
	REQUIRE(book.get_cost() == 0);				// - 30
	REQUIRE(book.get_qty() == 150);				// - 31
	REQUIRE(book.get_qtySold() == 0);			// - 32

	REQUIRE(book.get_ISDN() == "AD009");		// - 33

}


TEST_CASE("test Product Class", "[Product()]")
{
	CD item;
	Product* prod = &item;

	prod->set_id(9);
	prod->set_title("pp");
	prod->set_cost(60);
	prod->set_qty(100);
	prod->set_qtySold(4);

	//REQUIRE(prod->get_type() == "");
	REQUIRE(prod->get_id() == 9);				// - 34
	REQUIRE(prod->get_title() == "pp");			// - 35
	REQUIRE(prod->get_cost() == 60);			// - 36
	REQUIRE(prod->get_qty() == 100);			// - 37
	REQUIRE(prod->get_qtySold() == 4);			// - 38

	REQUIRE(item.get_qty() == 100);				// - 39
	REQUIRE(item.get_qtySold() == 4);			// - 40
}
