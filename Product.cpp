#include "Product.h"
#include <iostream>

Product::Product()
{
    this->type = "";
    this->id = 0;
    this->title = "";
    this->cost = 0.0;
    this->qty = 0;
    this->qtySold = 0;
}

Product::Product(std::string type, int id, std::string title, double cost,
    int qty, int qtySold)
{
    this->type = type;
    this->id = id;
    this->title = title;
    this->cost = cost;
    this->qty = qty;
    this->qtySold = qtySold;
}

// Getters
int Product::get_id() {    return id; }
std::string Product::get_type() { return type; }
std::string  Product::get_title() { return title; }
double Product::get_cost() { return cost; }
int Product::get_qty() { return qty; }
int Product::get_qtySold() { return qtySold; }

// Setters
void Product::set_id(int id) { this->id = id; }
void Product::set_type(std::string type) { this->type = type; }
void Product::set_title(std::string title) { this->title = title; }
void Product::set_cost(double cost) { this->cost = cost; }
void Product::set_qty(int qty) { this->qty = qty; }
void Product::set_qtySold(int qtySold) { this->qtySold = qtySold; }
