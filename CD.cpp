#include "CD.h"
#include <sstream>

CD::CD() :Product()
{
    this->artist = "";
    this->truck_count = 0;
    this->play_time = 0;
    this->genre = "";
}
CD::CD(std::string type, int id, std::string title, double cost, int qty, int qtySold,
    std::string artist, int truck_count, double play_time, std::string genre)
    :Product(type, id, title, cost, qty, qtySold)
{
    this->artist = artist;
    this->truck_count = truck_count;
    this->play_time = play_time;
    this->genre = genre;
}


// Seters
std::string CD::get_artist(){return this->artist;}
int CD::get_truck_count() { return this->truck_count; }
double CD::get_play_time() { return this->play_time; }
std::string CD::get_genre() { return this->genre; }
// Getters
void CD::set_artist(std::string artist) { this->artist = artist; }
void CD::set_truck_count(int truck_count) { this->truck_count = truck_count; }
void CD::set_play_time(double play_time) { this->play_time = play_time; }
void CD::set_genre(std::string genre) { this->genre = genre; }

void CD::writeNewProduct()
{
    int id = 0;
    std::cout << "Enter item id : ";
    std::cin >> id;
    
    bool found = false;
    std::ifstream inFile("stock.txt");
    std::string arr[10];
    std::string line;
    while (getline(inFile, line))
    {
        if (line.length()<1) 
        {
            break; 
        }
        
        std::istringstream iss(line);
        for (int i = 0; i < (sizeof(arr) / sizeof(arr[0])); i++)
        {
            std::string subs;
            iss >> subs;
            arr[i] = subs;
        }

        if (std::to_string(id) == arr[1])
        {
            std::cout << "\n\tThe Product with id '" << id << "' is found already!\n";
            found = true;
            break;
        }
    }
    inFile.close();

    if (found == false)
    {   
        std::string type = "CD";
        std::string title, artist, genre;
        int qty, qtySold, truck_count;
        double cost, play_time; 
        std::cout << "Enter item title : ";
        std::cin >> title;
        std::cout << "Enter item cost : ";
        std::cin >> cost;
        std::cout << "Enter item Qty : ";
        std::cin >> qty;
        std::cout << "Enter item Qty Sold : ";
        std::cin >> qtySold;
        //=========================
        std::cout << "Enter artist : ";
        std::cin >> artist;
        std::cout << "Enter number of tracks : ";
        std::cin >> truck_count;
        std::cout << "Enter playing time for CD: ";
        std::cin >> play_time;
        std::cout << "Enter genre : ";
        std::cin >> genre;

        std::ofstream outFile;
        outFile.open("stock.txt", std::ios::out | std::ios::app);

        outFile << type << "\t" <<id<< "\t" << title << "\t" <<cost << "\t" << qty << "\t" << qtySold 
            <<"\t"<<artist<<"\t" << truck_count<<"\t"<< play_time << "\t"<< genre <<"\n";

        outFile.close();
        std::cout << "\nProduct '" << title<< "' written successfully!\n\n";
    }
}

void CD::updateStockItem(int id, int n, std::string qty_arg)
{
    bool found = false;
    std::ifstream inFile("stock.txt");
    std::ofstream tempFile("temp.txt");
    std::string arr[10];
    std::string line;
    while (getline(inFile, line))
    {
        if (line.length() < 1)
        {
            break;
        }

        std::istringstream iss(line);
        for (int i = 0; i < (sizeof(arr) / sizeof(arr[0])); i++)
        {
            std::string subs;
            iss >> subs;
            arr[i] = subs;
        }
        if (id != stoi(arr[1]) && line.length() >1)
            tempFile << line<<"\n";
        else if (id == stoi(arr[1])  && line.length() > 1)
        {
            int x = stoi(arr[4]);
            int y = stoi(arr[5]);
            found = true;
            if (qty_arg == "qty")
            {
                x += n;
                arr[4] = std::to_string(x);
            }
            else if (qty_arg == "qtySold")
            {
                x -= n;
                y += n;
                arr[4] = std::to_string(x);
                arr[5] = std::to_string(y);
            }
            tempFile << arr[0] << "\t" << arr[1] << "\t" << arr[2] << "\t" << arr[3] << "\t" << arr[4] << "\t" << arr[5]
                << "\t" << arr[6] << "\t" << arr[7] << "\t" << arr[8] << "\t" << arr[9] << "\n";

        }
    }
    inFile.close();
    tempFile.close();

    //======================================

    if (found == true)
    {
        remove("stock.txt");
        int c = rename("temp.txt", "stock.txt");
    }
    else
    {
        std::cout << "\nThe item which has id '" << id << "' not found\n";
        remove("temp.txt");
    }
        
}

