//#pragma once
#ifndef PRODUCT_H
#define PRODUCT_H
#include <iostream>
#include <string>
#include <vector>

class Product
{
private:
    std::string type;
    int id;
    std::string title;
    double cost;
    int qty;
    int qtySold;
public:
    Product();
    Product(std::string type, int id, std::string title, double cost, int qty, int qtySold);
    //~Product();
  
    virtual void writeNewProduct() = 0;
    virtual void updateStockItem(int id, int n, std::string qty_arg) = 0;

    // Getters
    int get_id();
    std::string get_type();
    std::string  get_title();
    double get_cost();
    int get_qty();
    int get_qtySold();
    // Setters
    void set_id(int id);
    void set_type(std::string type);
    void set_title(std::string title);
    void set_cost(double cost);
    void set_qty(int qty);
    void set_qtySold(int qtySold);
};

#endif // PRODUCT_H
