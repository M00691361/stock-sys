//#pragma once
#ifndef MAGAZINE_H
#define MAGAZINE_H
#include "Product.h"
#include <fstream>

class Magazine : public Product
{
private:
    int issue_number;
public:
    Magazine();
    //~Magazine();
    Magazine(std::string type, int id, std::string title, double cost, int qty, int qtySold,
        int issue_number);
    void writeNewProduct();
    void updateStockItem(int id, int n, std::string qty_arg);

    // Getter
    int get_issue_number();
  
    // Setter
    void set_issue_number(int issue_number);
};
#endif 
