//#pragma once
#ifndef CD_H
#define CD_H
#include "Product.h"
#include <fstream>

class CD : public Product
{
private:
    std::string artist;
    int truck_count;
    double play_time;
    std::string genre;
public:
    CD();
    //~CD();
    CD(std::string type, int id, std::string title, double cost, int qty, int qtySold,
        std::string artist, int truck_count, double play_time, std::string genre);
  
    void writeNewProduct();
    void updateStockItem(int id, int n, std::string qty_arg);
    
    // Getters
    std::string get_artist();
    int get_truck_count();
    double get_play_time();
    std::string get_genre();
    // Setters
    void set_artist(std::string artist);   
    void set_truck_count(int truck_count);
    void set_play_time(double play_time);
    void set_genre(std::string genre);


};

#endif // CD_H
