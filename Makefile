CXX = g++
CXXFLAGS = -g -Wall -Wextra
.PHONY : all
all : StockMS
StockMS : StockMS.cpp CD.o DVD.o Book.o Magazine.o Product.o
	$(CXX) $(CXXFLAGS) -o StockMS StockMS.cpp CD.o DVD.o Book.o Magazine.o Product.o

CD.o : CD.cpp CD.h Product.o
	$(CXX) $(CXXFLAGS) -c CD.cpp Product.o

DVD.o : DVD.cpp DVD.h Product.o
	$(CXX) $(CXXFLAGS) -c DVD.cpp Product.o

Magazine.o : Magazine.cpp Magazine.h Product.o
	$(CXX) $(CXXFLAGS) -c Magazine.cpp Product.o

Book.o : Book.cpp Book.h Product.o
	$(CXX) $(CXXFLAGS) -c Book.cpp Product.o

Product.o : Product.cpp Product.h
	$(CXX) $(CXXFLAGS) -c Product.cpp

.PHONY : clean
clean :
	rm *.o
	rm StockMS
