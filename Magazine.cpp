#include "Magazine.h"
#include <sstream>

Magazine::Magazine() :Product()
{
    issue_number = 0;
}
Magazine::Magazine(std::string type, int id, std::string title, double cost, int qty, int qtySold,
    int issue_number) : Product("Magazine", id, title, cost, qty, qtySold)
{
    this->issue_number = issue_number;
}

//Magazine::~Magazine(){    //cout << "P destructor\n";}

// Getter
int Magazine::get_issue_number() { return this->issue_number; }

// Setter
void Magazine::set_issue_number(int issue_number) { this->issue_number = issue_number; }

void Magazine::writeNewProduct()
{
    int id;
    std::cout << "Enter item id : ";
    std::cin >> id;

    bool found = false;
    //string pType = " ";
    std::ifstream inFile("stock.txt");
    std::string arr[6];
    std::string line;
    while (getline(inFile, line))
    {
        if (line.length() < 1)
        {
            break;
        }

        std::istringstream iss(line);
        for (int i = 0; i < (sizeof(arr) / sizeof(arr[0])); i++)
        {
            std::string subs;
            iss >> subs;
            arr[i] = subs;
        }

        if (std::to_string(id) == arr[1])
        {
            std::cout << "\n\tThe Product with id '" << id << "' is found already!\n";
            found = true;
            break;
        }
    }
    inFile.close();

    if (found == false)
    {
        std::string type = "Magazine";
        std::string title ;
        int qty, qtySold,issue_number;
        double cost;

        std::cout << "Enter item title : ";
        std::cin >> title;
        std::cout << "Enter item cost : ";
        std::cin >> cost;
        std::cout << "Enter item Qty : ";
        std::cin >> qty;
        std::cout << "Enter item Qty Sold : ";
        std::cin >> qtySold;
        //=======================
        std::cout << "Enter issue_number   : ";
        std::cin >> issue_number;


        std::ofstream outFile;
        outFile.open("stock.txt", std::ios::out | std::ios::app);

        outFile << type << "\t" << id << "\t" << title << "\t" << cost << "\t" << qty << "\t" << qtySold
            << "\t" << issue_number << "\n";

        outFile.close();
        std::cout << "\nProduct '" << title << "' written successfully!\n\n";
    }
}

void Magazine::updateStockItem(int id, int n, std::string qty_arg)
{
    bool found = false;
    std::ifstream inFile("stock.txt");
    std::ofstream tempFile("temp.txt");
    std::string arr[10];
    std::string line;
    while (getline(inFile, line))
    {
        if (line.length() < 1)
        {
            break;
        }

        std::istringstream iss(line);
        for (int i = 0; i < (sizeof(arr) / sizeof(arr[0])); i++)
        {
            std::string subs;
            iss >> subs;
            arr[i] = subs;
        }
        if (std::to_string(id) != arr[1] && line.length() > 1)
            tempFile << line << "\n";
        else if (std::to_string(id) == arr[1] && line.length() > 1)
        {
            int x = stoi(arr[4]);
            int y = stoi(arr[5]);
            found = true;
            if (qty_arg == "qty")
            {
                x += n;
                arr[4] = std::to_string(x);
            }
            else if (qty_arg == "qtySold")
            {
                x -= n;
                y += n;
                arr[4] = std::to_string(x);
                arr[5] = std::to_string(y);
            }
            tempFile << arr[0] << "\t" << arr[1] << "\t" << arr[2] << "\t" << arr[3] << "\t" << arr[4] << "\t" << arr[5]
                << "\t" << arr[6] << "\n";
        }
    }
    inFile.close();
    tempFile.close();

    //======================================

    if (found == true)
    {
        remove("stock.txt");
        int c = rename("temp.txt", "stock.txt");
    }
    else
    {
        std::cout << "\nThe item which has id '" << id << "' not found\n";
        remove("temp.txt");
    }
}

