//#pragma once
#ifndef BOOK_H
#define BOOK_H
#include "Product.h"
#include <fstream>

class Book : public Product
{
private:
    std::string ISDN;
public:
    Book();
    Book(std::string type, int id, std::string title, double cost, int qty, int qtySold, std::string ISDN);
    void writeNewProduct();
    void updateStockItem(int id, int n, std::string qty_arg);


    // Getter
    std::string get_ISDN();
  
    // Setter
    void set_ISDN(std::string ISDN);
};
#endif 
